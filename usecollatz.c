#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include<math.h>

void ChildProcess(); //Child Process prototype
void ParentProcess(); //Parent process prototype

int main(void)
{
  char* n[3];//Passing parameters for exec
  char i[5];//Holds "string" for n
  int c;//Used for conditional
  n[0] = "./collatz";//execv command
  n[2] = NULL;
  n[1] = "13"; // Dummy argument to test execv
  printf("\nPlease enter a number 1-100: ");
  scanf("%s",&i);//User input
  sscanf(i,"%d",&c);//Convers char array into a single int
  
  while((c > 100) | (c <= 0))//Anything less than 1 will not perform collatz correctly, 100 simply for the assignment
    {
      printf("\n That is not a valid number.\nPlease enter a number 1-100:");
      scanf("%s",&i);
      sscanf(i,"%d",&c);
     }
  n[1] = &i;
  pid_t pid;
  pid = fork();
  if (pid == 0)
    {
    ChildProcess(n);
    }
  else
    ParentProcess(i);
  return 0;
}
void ChildProcess(char* argv[])
{
  char* test[] = {"./collatz","5",NULL};
  printf("Child is performing processes...\n");
  execv("./collatz", argv);
 
}
void ParentProcess(char n[])
{
  printf("\nThis is the usecollatz start\nYour number is %s\n",n);
  wait();
  printf("\nThis is the usecollatz END\n");
 
}
