#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include<math.h>

void ChildProcess(); //Child Process prototype
void ParentProcess(); //Parent process prototype

int main(int argc, char **argv)
{
  int n;
  pid_t pid;
  pid = fork();
  n = atoi(argv[1]);
  if (pid == 0)
    {
    ChildProcess(n);
    }
  else
    ParentProcess();
  return 0;
}
void ChildProcess(int n)
{
  printf("Child is performing processes...\n");
  printf("%d\n",n);
    while (n!=1)
      {
	if(n%2 == 0)
	  {
	    n = n/2;
	  }
	else if (n%2 == 1)
	  {
	    n = 3*n+1;
	  }
	printf("%d\n",n);
      }
}
void ParentProcess()
{
  printf("START\n");
  wait();
  printf("END\n");
}
